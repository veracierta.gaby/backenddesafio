﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cursos.Models
{
    public partial class Casas
    {
        public Casas()
        {
            InscripcionCurso = new HashSet<InscripcionCurso>();
        }

        [Key]
        public int IdCasas { get; set; }


        [ForeignKey("IdCasas")]
        public virtual Casas Casas { get; set; }

        public virtual ICollection<Tipo_Casas> Tipo_Casas {get; set;}
    
        public virtual ICollection<InscripcionCurso> InscripcionCurso { get; set; }

        [HttpGet]
        public async Task<IEnumerable<Tipo_Casas>> Get()
        {
            return await ctx.Casas.ToListAsync();
        }
    }
    }
}
