﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cursos.Models
{
    public partial class Estudiante
    {
        public Estudiante()
        {
            Casas = new HashSet<Casas>();
        }

        [Key]
        public int IdEstudiante { get; set; }

        [StringLength(10)]
        [Required(ErrorMessage = "El código es obligatorio.")]
        [MinLength(10, ErrorMessage = "El código debe ser mínimo de 10 caracteres.")]
        [MaxLength(10, ErrorMessage = "El código debe ser máximo de 10 caracteres.")]
        public string identificacion { get; set; }

        [StringLength(20)]
        [Required(ErrorMessage = "El nombre es obligatorio.")]
        [MinLength(3, ErrorMessage = "El nombre debe ser mínimo de 3 caracteres.")]
        [MaxLength(20, ErrorMessage = "El nombre debe ser máximo de 20 caracteres.")]
        public string Nombre { get; set; }

        [StringLength(20)]
        [Required(ErrorMessage = "El nombre es obligatorio.")]
        [MinLength(3, ErrorMessage = "El apellido debe ser mínimo de 3 caracteres.")]
        [MaxLength(20, ErrorMessage = "El apellido debe ser máximo de 20 caracteres.")]
        public string Apellido { get; set; }

        
        [StringLength(2)]
        [Required(ErrorMessage = "La edad  es obligatorio.")]
        [MinLength(2, ErrorMessage = "La edad  debe ser mínimo de 2 caracteres.")]
        [MaxLength(2, ErrorMessage = "La edad  debe ser máximo de 2 caracteres.")]
        public string Apellido { get; set; }


        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string NombreApellido { get; set; }


        [Column(TypeName = "date")]
        [Required(ErrorMessage = "La fecha de nacimiento es obligatoria.")]
        [DataType(DataType.Date, ErrorMessage = "La fecha no es válida.")]
        public DateTime? FechaNacimiento { get; set; }

        public virtual ICollection<Casas> Casas { get; set; }
    }
}
