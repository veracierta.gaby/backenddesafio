    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Cursos.Models;
    using Cursos.Helper;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore;
    using Newtonsoft.Json;
    using Microsoft.AspNetCore.JsonPatch;

namespace Cursos.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EstudiantesController:ControllerBase
    {
        
        private readonly CursosCTX ctx;

        public EstudiantesController(CursosCTX _ctx)
        {
            ctx = _ctx;
        }

        [HttpGet]
        public async Task<IEnumerable<Estudiante>> Get()
        {
            return await ctx.Estudiante.ToListAsync();
        }

        [HttpGet("{id}", Name="GetEstudiante")]
        public async Task<IActionResult> Get(int id, string identificacion)
        {
            var estudiante = await ctx.Estudiante.FindAsync(id);
            if(estudiante == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(estudiante);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Estudiante Estudiante)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ErrorHelper.GetModelStateErrors(ModelState));
            }
            else
            {
                if(await ctx.Estudiante.Where(x=>x.identificacion == Estudiante.identificacion) & x=>x.Estudiante.Where(x=>x.Nombre == Estudiante.Nombre)x=>x.Estudiante.Where(x=>x.Casa == Estudiante.Casa) ).AnyAsync())
                {
                    return BadRequest(ErrorHelper.Response(400, $"La identificación {Estudiante.identificacion} ya existe."));
                }

                Estudiante.IdEstudiante = 0;
                ctx.Estudiante.Add(Estudiante);
                await ctx.SaveChangesAsync();
                return CreatedAtRoute("GetEstudiante", new { id = Estudiante.IdEstudiante, identificacion = Estudiante.identificacion }, Estudiante);
            }
        }

        //context.Entry(todoItem).State = EntityState.Modified;

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Estudiante Estudiante)
        {
            if(Estudiante.IdEstudiante == 0)
            {
                Estudiante.IdEstudiante = id;
            }

            if(Estudiante.IdEstudiante != id)
            {
                return BadRequest(ErrorHelper.Response(400, "Petición no válida."));
            }
            
            if(!await ctx.Estudiante.Where(x=>x.IdEstudiante == id).AsNoTracking().AnyAsync())
            {
                return NotFound();
            }

            if(await ctx.Estudiante.Where(x=>x.identificacion == Estudiante.identificacion && x.IdEstudiante != Estudiante.IdEstudiante).AnyAsync())
            {
                return BadRequest(ErrorHelper.Response(400, $"El código {Estudiante.identificacion} ya existe."));
            }

            ctx.Entry(Estudiante).State = EntityState.Modified;
            if(!TryValidateModel(Estudiante, nameof(Estudiante)))
            {
                return BadRequest(ErrorHelper.GetModelStateErrors(ModelState));
            }
            
            await ctx.SaveChangesAsync();
            return NoContent();
        }

        [HttpPatch("Cambiaridentificacion/{id}")]
        public async Task<IActionResult> Cambiaridentificacion(int id, [FromQuery] string identificacion)
        {

            if(string.IsNullOrWhiteSpace(identificacion))
            {
                return BadRequest(ErrorHelper.Response(400, "La identificación está vacío."));
            }

            var Estudiante = await ctx.Estudiante.FindAsync(id);
            if(Estudiante == null)
            {
                return NotFound();
            }

            if(await ctx.Estudiante.Where(x=>x.identificacion == identificacion && x.IdEstudiante != id).AnyAsync())
            {
                return BadRequest(ErrorHelper.Response(400, $"La identificación {identificacion} ya existe."));
            }

            Estudiante.identificacion = identificacion;

            if(!TryValidateModel(Estudiante, nameof(Estudiante)))
            {
                return BadRequest(ErrorHelper.GetModelStateErrors(ModelState));
            }

            await ctx.SaveChangesAsync();
            return StatusCode(200, Estudiante);
        }

//delete estudiantes inscritos
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var Estudiante = await ctx.Estudiante.FindAsync(id);
            if(Estudiante == null)
            {
                return NotFound();
            }

            ctx.Estudiante.Remove(Estudiante);
            await ctx.SaveChangesAsync();
            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, JsonPatchDocument<Estudiante> _Estudiante)
        {
            var Estudiante = await ctx.Estudiante.FindAsync(id);
            if(Estudiante == null)
            {
                return NotFound();
            }

            _Estudiante.ApplyTo(Estudiante, ModelState);
            if(!TryValidateModel(Estudiante, "Estudiante"))
            {
                return BadRequest(ErrorHelper.GetModelStateErrors(ModelState));
            }
            await ctx.SaveChangesAsync();
            return Ok(Estudiante);
        }
    }
    
}